// zad. 1
func printDigit() {

    for i in 1 ... 100 {

        let modulo_two = 2
        let modulo_five = 5

        if i % modulo_two == 0 {
            print("\(i) podzielna przez 2")
        }
        else if i % modulo_five == 0 {
            print("\(i) podzielna przez 5")
        } else {
            print(i)
        }
    }
}

printDigit()
print("----------------------------- Zad 2 ------------------------")

/////////////////////////////////////////////////////////////////////////////////////////////////////
// zad.2
func newPrintDigit (digit: Int = 100) {

    for i in 1 ... digit {

        let modulo_two = 2
        let modulo_five = 5

        if i % modulo_two == 0 {
            print("\(i) podzielna przez 2")
        }
        else if i % modulo_five == 0 {
            print("\(i) podzielna przez 5")
        } else {
            print(i)
        }
    }
}

newPrintDigit()
/////////////////////////////////////////////////////////////////////////////////////////////////////
// zad. 3
var tableFirst = ["first","second", "third"]

print(tableFirst)

func reverseArray(array: [String]) -> [String] {

    let newArray = array.sorted{ $0 > $1 }
    return newArray
}

print("----------------------------- Zad 3 ------------------------")
let newArray = reverseArray(array: tableFirst)
print(newArray)
