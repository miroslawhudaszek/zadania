
import Foundation

class NetworkHelper: NSObject {

    func getUserList(callback:@escaping ([String])->Void){
        let address = "https://api.github.com/users?since=135"
        let url = NSURL(string: address)
        let request = NSURLRequest(url: url! as URL)

        let ses = URLSession.shared.dataTask(with:request as URLRequest) {(data,response,error) in
            if error != nil{
                return
            }
            do {
                
                let table = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String]

                callback(table!)


            } catch let error {
                print(error)
            }
        }
        
        ses.resume()
    }
}
